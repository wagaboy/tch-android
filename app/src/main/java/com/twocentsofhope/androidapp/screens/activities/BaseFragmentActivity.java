package com.twocentsofhope.androidapp.screens.activities;

import android.support.v4.app.FragmentActivity;

import com.twocentsofhope.androidapp.R;
import com.twocentsofhope.androidapp.logic.SalesforceService;

import retrofit.RestAdapter;

/**
 * Base activity that supports fragments.
 */
public abstract class BaseFragmentActivity extends FragmentActivity {
    RestAdapter salesforceRestAdapter;
    SalesforceService salesforceService;

    @Override
    protected void onStart() {
        super.onStart();
        salesforceRestAdapter = new RestAdapter.Builder()
                .setEndpoint(getString(R.string.salesforce_endpoint))
                .build();
        salesforceService = salesforceRestAdapter.create(SalesforceService.class);
    }

    protected SalesforceService getSalesforceService() {
        return salesforceService;
    }

}
