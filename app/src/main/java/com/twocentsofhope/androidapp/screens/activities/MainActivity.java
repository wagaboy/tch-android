package com.twocentsofhope.androidapp.screens.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.twocentsofhope.androidapp.R;

/**
 * MainActivity is sort of the landing page after user login
 */
public class MainActivity extends BaseFragmentActivity {

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
