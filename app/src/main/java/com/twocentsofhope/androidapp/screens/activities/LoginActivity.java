package com.twocentsofhope.androidapp.screens.activities;

import android.app.Dialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SignInButton;
import com.twocentsofhope.androidapp.R;

import static com.google.android.gms.common.GooglePlayServicesUtil.isGooglePlayServicesAvailable;


public class LoginActivity extends BaseFragmentActivity implements View.OnClickListener {

    // Use DUMMY_REQUEST_CODE for activity results we don't care about
    private static final int DUMMY_REQUEST_CODE = 1001;

    private SignInButton btnGoogleSignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setupUi();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupUi() {
        btnGoogleSignIn = (SignInButton) findViewById(R.id.btnGoogleSignIn);
        btnGoogleSignIn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.btnGoogleSignIn:
                handleGoogleSignIn();
                break;
            default:
                throw new IllegalStateException("On noes! View with id=" + view.getId()
                        + " doesn't have a handler. view_details=" + view);
        }
    }

    /**
     * Handles Google SignIn
     */
    private void handleGoogleSignIn() {
        // Prompt installation of Google Play Services if it's not already installed.
        // We need this service for Google SignIn.
        int resultCode = isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            Dialog dialog
                    = GooglePlayServicesUtil.getErrorDialog(resultCode, this, DUMMY_REQUEST_CODE);
            dialog.show();
        }

        // TODO Follow Google Signin instructions provided here:
        // https://developers.google.com/+/mobile/android/sign-in

        // For now we assume Google Sign-in is successful and launch the main activity
        loginSuccess();
    }

    /** Things to do after successful login */
    private void loginSuccess() {
        MainActivity.startActivity(this);
        finish(); // Pop login activity off activity stack
    }
}
