package com.twocentsofhope.androidapp;

import android.app.Application;
import android.content.Context;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;

/**
 * TchApp is the Android Application class for TCH.
 */
public class TchApp extends Application {

    /**
     * A helper method to return the TchApp from a given Android Context
     * @param context the android context
     * @return the TchApp instance
     */
    public static TchApp get(Context context) {
        return (TchApp) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        setupCookieManager();
    }

    /**
     * Ensure we persist cookies in HTTP clients.
     */
    private void setupCookieManager() {
        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        CookieHandler.setDefault(cookieManager);
    }
}
